import { CmdAst } from './cmdast';
export declare module CmdScript {
    class Environment implements CmdAst.IEnvironment {
        _vars: {
            [key: string]: string;
        };
        pushState(): void;
        popState(): void;
        endLocal(): void;
        isEndLocal(): boolean;
        get(key: string): string;
        set(key: string, val: any): void;
    }
    class Script {
        private _text;
        private _rdr;
        private _labels;
        constructor(text: string);
        goto(label: string): void;
        readNextLine(env: Environment): string;
        processPercent(env: Environment): string;
    }
}
