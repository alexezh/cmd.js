import { CmdAst } from './cmdast'

export module CmdScript {

    export class Environment implements CmdAst.IEnvironment {
        _vars: { [key: string]: string } = {};

        public pushState() {

        }

        public popState() {

        }

        public endLocal() {

        }

        public isEndLocal(): boolean {
            return false;
        }

        public get(key: string): string {
            let val = this._vars[key];
            return val;
        }

        public set(key: string, val: any) {
            this._vars[key] = val;
        }
    }

    class TextReader {
        _text: string;
        _pos = 0;

        constructor(text: string) {
            this._text = text;
        }

        public readNext(): string {
            if (this._pos == this._text.length) {
                return '\0';
            }

            let c = this._text[this._pos];
            this._pos++;

            // replace \r\n with \n; keep single \r
            if (c == '\r') {
                if (this._pos == this._text.length)
                    return c;

                if (this._text[this._pos] == '\n') {
                    this._pos++;
                    return '\n';
                }
            }

            return c;
        }

        public peekNext(): string {
            if (this._pos == this._text.length) {
                return '\0';
            }

            // skip \r\n handling here
            return this._text[this._pos];
        }

        public isEnd() {
            return (this._pos == this._text.length);
        }

        public pos() {
            return this._pos;
        }

        public reset() {
            this._pos = 0;
        }

        public scan(r: RegExp): boolean {
            let res = r.exec(this._text);
            if (res == null)
                return false;

            this._pos = r.lastIndex;

            return true;
        }
    }

    // read script line by line.
    export class Script {
        private _text: string;
        private _rdr: TextReader;

        // cache position of labels
        private _labels: { [label: string]: number };

        constructor(text: string) {
            this._text = text;
            this._rdr = new TextReader(text);
        }

        // move reader to position after label 
        goto(label: string) {
            this._rdr.reset();
            if (!this._rdr.scan(/[\s&]*\:[a-zA-Z0-9]+/))
                throw "The system cannot find the batch label specified - " + label;
        }

        // read next line. Handles ^ and() as well as variable expansion
        // see http://stackoverflow.com/questions/4094699/how-does-the-windows-command-interpreter-cmd-exe-parse-scripts/4095133#4095133
        readNextLine(env: Environment): string {
            let line = "";
            while (!this._rdr.isEnd()) {
                let c = this._rdr.readNext();
                switch (c) {
                    case '^': {
                            let c1 = this._rdr.readNext();
                            if (c1 == '\0') {
                                break;
                            }

                            // if non-special char, skip caret
                            if (c1 != '\n') {
                                line += c;
                                continue;
                            }

                            // for \n, read next char and treat it as escaped
                            let c2 = this._rdr.readNext();
                            line += c2;
                        }
                        break;
                    case '\n':
                        return line;
                    case '%':
                        line += this.processPercent(env);
                        break;
                    default:
                        line += c;
                }
            }

            if (line.length > 0) {
                return line;
            }

            return null;
        }

        processPercent(env: Environment): string {
            let name = "";
            while (!this._rdr.isEnd()) {
                let c = this._rdr.readNext();
                if (c == '%')
                    break;

                name += c;
            } 

            if (name.length == 0)
                return '%';

            let val = env.get(name);
            if (val == undefined)
                return name;

            return val;
        }
    }
}
