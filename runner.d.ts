import { CmdScript } from './cmdscript';
import { CmdParser } from './cmdparser';
import { CmdAst } from './cmdast';
export declare module CmdRunner {
    class Runner implements CmdAst.IRunner {
        _env: CmdScript.Environment;
        _script: CmdScript.Script;
        _parser: CmdParser.Parser;
        constructor(text: string);
        run(): void;
        call(label: string): void;
        exec(env: CmdAst.IEnvironment, cmd: string, args?: string[], stdio?: CmdAst.IPipe[]): CmdAst.ITask;
        createPipe(): CmdAst.IPipe;
        private processLine(line);
    }
}
