/// <reference path="defs/lib.es6.d.ts" />
import { CmdLexer } from './cmdlexer';
export declare module CmdAst {
    import Token = CmdLexer.Token;
    interface IPipe {
    }
    interface ITask {
    }
    interface IRunner {
        call(label: string): any;
        exec(env: CmdAst.IEnvironment, cmd: string, args?: string[], stdio?: IPipe[]): CmdAst.ITask;
        createPipe(): IPipe;
    }
    interface IEnvironment {
        pushState(): any;
        popState(): any;
        endLocal(): any;
        isEndLocal(): boolean;
        get(key: string): string;
        set(key: string, val: any): any;
    }
    class Ast {
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class LineAst extends Ast {
        children: Ast[];
        run(runner: IRunner, env: IEnvironment): Promise<any>;
        runWorker(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class ExecFunc {
        func: (pipes?: IPipe[]) => Promise<ITask>;
        constructor(func: (pipes: IPipe[]) => Promise<ITask>);
    }
    class TaskAst extends Ast {
        root: boolean;
        run(runner: IRunner, env: IEnvironment): Promise<any>;
        runCore(runner: IRunner, env: IEnvironment): ExecFunc;
    }
    class CommandAst extends TaskAst {
        cmd: string;
        tokens: string[];
        runCore(runner: IRunner, env: IEnvironment): ExecFunc;
    }
    class ForAst extends Ast {
        inputSet: Ast;
        body: Ast;
        var: string;
        run(runner: IRunner, env: IEnvironment): Promise<any>;
        private runWorker(runner, env);
    }
    class IfAst extends Ast {
    }
    class SetAst extends Ast {
        variable: string;
        expression: string;
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class SetLocalAst extends Ast {
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class EndLocalAst extends Ast {
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class VarAst extends Ast {
        text: string;
        constructor(text: string);
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class GotoAst extends Ast {
        label: string;
        constructor(text: string);
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class ConstAst extends Ast {
        value: string;
        constructor();
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class CallAst extends Ast {
        label: string;
        external: string;
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
    class ScopeAst extends Ast {
        expressions: Ast[];
    }
    class PipeAst extends TaskAst {
        left: Ast;
        right: Ast;
        constructor();
        run(runner: IRunner, env: IEnvironment): Promise<any>;
        execWorker(runner: IRunner, env: IEnvironment, stdio?: IPipe[]): Promise<ITask>;
    }
    class RedirectAst extends TaskAst {
    }
    class ChainAst extends TaskAst {
        op: Token;
        source: Ast;
        dest: Ast;
        constructor(dest: Token);
        run(runner: IRunner, env: IEnvironment): Promise<any>;
    }
}
