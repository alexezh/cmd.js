export module CmdLexer {

    export enum TokenId {
        Id, // define variable
        Var, // %something%
        For,
        If,
        Set,
        SetLocal,
        EndLocal,
        Do,
        String,
        In,
        Chain, // &&, &, ||, 
        Pipe, //  <, >>, >, |
        Equal, // =
        Eol,
        LeftParent,
        RightParent,
        Option, // /D or similar
        Params, // %0 - %9 or %*
    }

    export class Token {
        constructor(text: string, id: TokenId, pos: number) {
            this.id = id;
            this.text = text;
            this.pos = pos;
        }

        id: TokenId;
        text: string;
        pos: number;
    }

    class Rule {
        constructor(text: string, id: TokenId) {
            this.text = text;
            this.token = id;
        }

        text: string;
        token: TokenId;
    }

    export class Lexer {
        private _source: string;
        private _next: Token;
        // position for next match
        private _pos: number = 0;
        private _end: boolean = false;

        private _matcher: RegExp;
        private _rules = [
            new Rule("set", TokenId.Set),
            new Rule("%.+?%", TokenId.Var),
            new Rule("\".*?\"", TokenId.String),
            new Rule("[a-zA-Z0-9]+", TokenId.Id),
            new Rule("=", TokenId.Equal),
            new Rule("\r?\n", TokenId.Eol),
            new Rule("%[0-9*]", TokenId.Params),
            new Rule("\\\".*\\\"", TokenId.String), // ignore escaped " for now
        ];

        constructor(line: string) {
            this._source = line;
            this.buildMatcher();
        }

        readNext(): Token {
            if (this._next != null) {
                let next = this._next;
                this._next = null;
                return next;
            }

            return this.readNextWorker();
        }
        readNextIf(id: TokenId): Token {
            let token = this.readNext();
            if (token.id != id) {
                throw "Invalid syntax";
            }
            return token;
        }
        peekNext(): Token {
            if (this._next == null) {
                this._next = this.readNextWorker();
            }
            return this._next;
        }
        isEnd(): boolean {
            if (this._next == null && !this._end) {
                this._next = this.readNextWorker();
            }
            return (this._next == null && this._end);
        }
        // read string starting from the current position until match
        readString(match: string): string {
            let exp = new RegExp("/" + match + "/");
            // use either current position or position of next token
            let pos = (this._next == null) ? this._pos : this._next.pos
            exp.lastIndex = pos;
            let res = exp.exec(this._source);
            let len = (res == null) ? this._source.length - pos : exp.lastIndex - pos;
            this._pos += len;

            if (this._pos == this._source.length)
                this._end = true;

            return this._source.substr(pos, len);
        }

        private buildMatcher() {
            let text = "^\\s*";
            let sep = false;
            for (var r of this._rules) {
                if (sep)
                    text += "|";

                text += "(" + r.text + ")";
                sep = true;
            }
            this._matcher = new RegExp(text, "ig");
        }

        private readNextWorker(): Token {
            let pos = this._pos;
            this._matcher.lastIndex = this._pos;
            var res = this._matcher.exec(this._source);
            if (res == null)
                throw "invalid syntax";

            // update our position
            this._pos = this._matcher.lastIndex;
            if (this._pos == this._source.length)
                this._end = true;

            for (let i = 0; i < this._rules.length; i++) {
                if (res[i + 1] != null) {
                    return new Token(res[0], this._rules[i].token, this._pos);
                }
            }

            throw "Not reached";
        }
    }
}
