import { CmdScript } from './cmdscript'
import { CmdParser } from './cmdparser'
import { CmdAst } from './cmdast'
import ChildProcess = require("child_process");

export module CmdRunner {
    class Pipe {
    }

    class WriteFileTask implements CmdAst.ITask {
        _stdin: Pipe;
        constructor(stdin: Pipe) {
            this._stdin = stdin;
        }
    }

    class RunAppTask implements CmdAst.ITask {
        _stdin: Pipe;
        _stdout: Pipe;
        _stderr: Pipe;

        constructor(stdin: Pipe, stdout: Pipe, stderr: Pipe) {
            this._stdin = stdin;
            this._stdout = stdout;
            this._stderr = stderr;
        }
    }

    export class Runner implements CmdAst.IRunner {
        _env: CmdScript.Environment;
        _script: CmdScript.Script;
        _parser: CmdParser.Parser;

        constructor(text: string) {
            this._env = new CmdScript.Environment();
            this._script = new CmdScript.Script(text);
            this._parser = new CmdParser.Parser();
        }

        // execute script until eol
        run() {
            while (true) {
                let line = this._script.readNextLine(this._env);
                if (line == undefined)
                    return;

                this.processLine(line);
            }
        }

        call(label: string) {
            ;
        }

        exec(env: CmdAst.IEnvironment, cmd: string, args?: string[], stdio?: CmdAst.IPipe[]) : CmdAst.ITask {
            let ps = ChildProcess.spawn(cmd, args, {
                detached: true,
                stdio: stdio
            });

/*
            ps.stdout.on('data', (data) => {
                grep.stdin.write(data);
            });

            ps.stderr.on('data', (data) => {
                console.log(`ps stderr: ${data}`);
            });

            ps.on('close', (code) => {
                if (code !== 0) {
                    console.log(`ps process exited with code ${code}`);
                }
                grep.stdin.end();
            });
*/
            return null;
        }

        createPipe(): CmdAst.IPipe {
            return null;
        }

        // at this point all macros in the line are resolved
        private processLine(line: string) {
            let ast: CmdAst.Ast;
            try {
                ast = this._parser.parse(line);
            }
            catch (e) {
                return;
            }

            // Get JavaScript representation of line and execute
            try {
                let res = ast.run(this, this._env);
            }
            catch (e) {
                // ignore for now
            }
        }
/*
        run(env: CmdScript.Environment, func: (runner: Runner, env: CmdScript.Environment, label: number) => number) {
            let id = 0;
            while (true) {
                id = func(this, env, id);
                if (id == -1)
                    break;
            }
        }

        // execute 
        execute(cmd: string, params: string[]) {

        }
*/
    }

}