/// <reference path="./defs/lib.es6.d.ts" />
import { CmdLexer } from './cmdlexer'

export module CmdAst {

    import Lexer = CmdLexer.Lexer;
    import Token = CmdLexer.Token;
    import TokenId = CmdLexer.TokenId;

    export interface IPipe {
    }

    export interface ITask {
    }

    export interface IRunner {
        call(label: string);
        // execute command; update error info if needed
        exec(env: CmdAst.IEnvironment, cmd: string, args?: string[], stdio?: IPipe[]): CmdAst.ITask;
        createPipe(): IPipe;
    }

    export interface IEnvironment {
        pushState();
        popState();
        endLocal();
        isEndLocal() : boolean;
        get(key: string): string;
        set(key: string, val: any);
    }

    type JsNode = (env: IEnvironment) => any;
    type RunComplete = () => void;
    export class Ast {
        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            return Promise.resolve(undefined)
	    }
    }

    // cmd executes operation line by line which affects variable visibility etc
    // 
    export class LineAst extends Ast {
	    public children : Ast[] = [];

        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            return this.runWorker(runner, env);
        }

        async runWorker(runner: IRunner, env: IEnvironment): Promise<any> {
            for (let node of this.children) {
                await node.run(runner, env);
		    }
            if (env.isEndLocal()) {
                env.popState();
            }
            return Promise.resolve(undefined);
	    }
    }

    export class ExecFunc {
        func: (pipes?: IPipe[]) => Promise<ITask>;

        constructor(func: (pipes: IPipe[]) => Promise<ITask>) {
            this.func = func;
        }
    }

    // base class for task like nodes producing ExecFunc
    export class TaskAst extends Ast {
        public root: boolean;
        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            let exec = this.runCore(runner, env);
            if (!this.root)
                return Promise.resolve(undefined);

            let task = exec.func();
            // wait for task to complete
            // task.
            return Promise.resolve(undefined);
        }

        runCore(runner: IRunner, env: IEnvironment): ExecFunc {
            return null;
        }
    }

    // command node manages execution of command or a set of commands
    // individual commands and handled by simple command. SimpleCommand generates a functor
    // as a result which is then wrapped by Pipe or Chain Ast object and at the end
    // executed by CommandAst
    export class CommandAst extends TaskAst {
        cmd: string;
        tokens: string[] = [];

        runCore(runner: IRunner, env: IEnvironment): ExecFunc {
            return new ExecFunc((stdio?: IPipe[]): Promise<ITask> => {
                // simply call runner
                return Promise.resolve(runner.exec(env, this.cmd, this.tokens, stdio));
            });
        }
    }

    export class ForAst extends Ast {
	    inputSet : Ast;
        body: Ast;
        var: string;

        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            return this.runWorker(runner, env);
        }

        private async runWorker(runner: IRunner, env: IEnvironment): Promise<any> {
            let set = await this.inputSet.run(runner, env);
            for (let item of set) {
                env.pushState();
                env.set(this.var, item);
                this.body.run(runner, env);
                env.popState();
            }
            return Promise.resolve(undefined);
	    }
    }

    export class IfAst extends Ast {

    }

    export class SetAst extends Ast {
	    variable : string;
	    expression : string;

        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            env.set(this.variable, this.expression);
            return Promise.resolve(undefined);
	    }
    }

    export class SetLocalAst extends Ast {
        // creates new environment state
        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            env.pushState();
            return undefined;
	    }
    }

    export class EndLocalAst extends Ast {
        // endlocal supports tunneling; any changes to environment done until EOL go to parent context
        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            env.endLocal();
            return Promise.resolve(undefined);
	    }
    }

    export class VarAst extends Ast {
        text: string;
        constructor(text: string) {
            super();
            this.text = text;
        }
        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            return Promise.resolve(env.get(this.text));
	    }
    }

    export class GotoAst extends Ast {
        label: string;

        constructor(text: string) {
            super();
            this.label = text;
        }

        public run(runner: IRunner, env: IEnvironment): Promise<any> {
            return Promise.resolve(this.label);
	    }
    }

    export class ConstAst extends Ast {
        value: string;
        constructor() {
            super();
        }

        run(runner: IRunner, env: IEnvironment): Promise<any> {
            return Promise.resolve(this.value);
        }
    }

    export class CallAst extends Ast {
	    label : string;
	    external : string;

	    // call runner with execute method or external app
	    // in case if we do not know, call external first
        run(runner: IRunner, env: IEnvironment): Promise<any> {
            runner.call(this.label);
            return Promise.resolve(undefined);
	    }
    }

    export class ScopeAst extends Ast {
	    expressions : Ast[];
    }

    // contains references to left and right commands
    export class PipeAst extends TaskAst {
	    left : Ast;
	    right : Ast;
        constructor() {
            super();
        }
        run(runner: IRunner, env: IEnvironment): Promise<any> {
            return Promise.resolve(new ExecFunc((stdio?: IPipe[]): Promise<ITask> => {
                return this.execWorker(runner, env, stdio);
            }));
        }

        async execWorker(runner: IRunner, env: IEnvironment, stdio?: IPipe[]): Promise<ITask>
        {
            // if left is not ExecFunc - ignore and execute right as is
            let leftFunc = await this.left.run(runner, env);
            if (!(leftFunc instanceof ExecFunc)) {
                if (this.right !== undefined) {
                    let rightFunc = await this.right.run(runner, env);
                    if (rightFunc instanceof ExecFunc) {
                        rightFunc.func();
                    }
                }
                return;
            }

            // if we have right part, get func out of it
            let rightFunc: ExecFunc = undefined;
            if (this.right !== undefined) {
                let res = await this.right.run(runner, env);
                if (res instanceof ExecFunc) {
                    rightFunc = <ExecFunc>res;
                }
            }

            // if right undefined; just execute left
            if (rightFunc === undefined) {
                leftFunc.func();
                return;
            }

            // now we have right and left. Get tasks and bind them
            let leftOut = runner.createPipe();
            let leftTask = leftFunc.func([null, leftOut, leftOut]);
            let rightTask = rightFunc.func(
                [leftOut,
                    (stdio != undefined) ? stdio[1] : null,
                    (stdio != undefined) ? stdio[2] : null]);

            return leftTask;
        }
    }

    export class RedirectAst extends TaskAst {
    }

    // chain multiple operations
    export class ChainAst extends TaskAst {
        op: Token;
        source: Ast;
        dest: Ast; 
        
        constructor(dest: Token) {
            super();
            this.op = dest;
        }
        run(runner: IRunner, env: IEnvironment): Promise<any> {
            // how do we tell outer layer. the only way to do this is to return
            // a functor.. 
            this.source.run(runner, env);

            return Promise.resolve(undefined);
        }
    }

}
