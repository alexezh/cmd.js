import { CmdLexer } from './cmdlexer'
import { CmdAst } from './cmdast'

export module CmdParser {
    import Token = CmdLexer.Token;
    import TokenId = CmdLexer.TokenId;

    enum ForInputSetKind {
        FileSet,
        Numbers,
        Content,
    }

    // command
    // command | command - first command opens with pipe out, second with pipe in
    // command && command - execute second IF first succeded
    // command & command - execute second THEN execute first
    // command || command - execute second IF first failed
    // () to test success of multiple commands
    // %x resolved before execution. % resolved dependent on context which for 
    export class Parser {
        lexer: CmdLexer.Lexer;

        parse(line: string): CmdAst.Ast {
            let ast = new CmdAst.LineAst();
            this.lexer = new CmdLexer.Lexer(line);

            while (!this.lexer.isEnd()) {
                ast.children.push(this.processExpression());
            }

            return ast;
        }

        processExpression(): CmdAst.Ast {
            let tok = this.lexer.readNext();

            switch (tok.id) {
                case TokenId.For: return this.processFor();
                case TokenId.If: return null;
                case TokenId.Set: return this.processSet();
                case TokenId.SetLocal: return null;
                default:
                    return this.processCommand(tok);
            }
        }

        processSet(): CmdAst.Ast {
            let ast = new CmdAst.SetAst();

            ast.variable = this.lexer.readNextIf(TokenId.Id).text;
            let eq = this.lexer.readNextIf(TokenId.Equal);

            // read the rest as a string 
            ast.expression = this.lexer.readString("&");

            return ast;
        }

        processCommand(tok: Token): CmdAst.Ast {
            var cmd = new CmdAst.CommandAst();

            // THIS logic is not right
            // read tokens until we find Eol or Redirect
            while (!this.lexer.isEnd()) {
                var tok = this.lexer.peekNext();
                if (tok.id == TokenId.Eol) {
                    this.lexer.readNext();
                    cmd.root = true;
                    return cmd;
                }
                else if (tok.id == TokenId.Chain) {
                    var chain = new CmdAst.ChainAst(tok);
                    chain.source = cmd;
                    chain.dest = this.processExpression();
                    this.setTaskCmdRoot(chain.dest, false);
                    this.setTaskCmdRoot(chain.source, false);
                    chain.root = true;
                    return chain;
                }
                else if (tok.id == TokenId.Pipe) {
                    var pipe = new CmdAst.PipeAst();
                    pipe.left = cmd;
                    pipe.right = this.processExpression();
                    this.setTaskCmdRoot(pipe.left, false);
                    this.setTaskCmdRoot(pipe.right, false);
                    pipe.root = true;
                    return pipe;
                } else {
                    this.lexer.readNext();
                    cmd.tokens.push(tok.text);
                }
            }

            return cmd;
        }

        setTaskCmdRoot(node: CmdAst.Ast, val: boolean) {
            if (!(node instanceof CmdAst.TaskAst))
                return;

            (<CmdAst.TaskAst>(node)).root = val;
        }



        // syntax-FOR - Files
        // FOR %%parameter IN (set) DO command 
        // syntax-FOR - Files - Rooted at Path   
        // FOR /R [[drive:]path] %%parameter IN (set) DO command 
        // syntax-FOR - Folders
        // FOR /D %%parameter IN (folder_set) DO command 
        // syntax-FOR - List of numbers   
        // FOR / L %%parameter IN (start, step, end) DO command 
        // syntax - FOR - File contents   
        // FOR / F["options"] %%parameter IN (filenameset) DO command 
        // FOR / F["options"] %%parameter IN ("Text string to process") DO command
        // syntax - FOR - Command Results 
        // FOR / F["options"] %%parameter IN ('command to process') DO command
        processFor(): CmdAst.Ast {
            var ast = new CmdAst.ForAst();
            var tok = this.lexer.readNext();
            let source: ForInputSetKind;
            if (tok.id == TokenId.Option) {
                var toku = tok.text.toUpperCase();
                if (toku == '/D') {
                    source = ForInputSetKind.FileSet;
                }
                else if (toku == '/F') {
                    source = ForInputSetKind.Content;
                }
                else if (toku == '/L') {
                    source = ForInputSetKind.Numbers;
                }
            }

            tok = this.lexer.readNext();
            if (tok.id != TokenId.Var)
                throw 'Invalid token ' + tok;

            ast.var = tok.text;

            // skip over IN
            tok = this.lexer.readNext();
            if (tok.id != TokenId.In)
                throw 'Invalid token ' + tok;

            switch (source) {
                case ForInputSetKind.FileSet:
                    ast.inputSet = this.processForFileSet(true, false);
                    break;
                case ForInputSetKind.Numbers:
                    ast.inputSet = this.processForNumbers();
                    break;
                case ForInputSetKind.FileSet:
                    ast.inputSet = this.processForContent();
                    break;
            }
            ast.body = this.processDo();

            return ast;
        }

        processForFileSet(files: boolean, recursive: boolean): CmdAst.Ast {
            return null;
        }
        processForNumbers(): CmdAst.Ast {
            return null;
        }
        processForContent(): CmdAst.Ast {
            return null;
        }

        processSetLocal(): CmdAst.Ast {
            var scope = new CmdAst.ScopeAst();
            while (!this.lexer.isEnd()) {
                var tok = this.lexer.peekNext();
                if (tok.id == TokenId.EndLocal) {
                    this.processEndLocal();
                    break;
                }
                else {
                    var child = this.processExpression();
                    scope.expressions.push(child);
                }
            }
            return scope;
        }

        processEndLocal(): CmdAst.Ast {
            var tok = this.lexer.peekNext();

            // handle tunneling
            if (tok.id == TokenId.Pipe) {
                if (tok.text != '&')
                    throw 'Invalid token ' + tok;

                // expression can read from inner context but sets to outer
                var right = this.processExpression();
            }

            return null;
        }

        processRedirect(): CmdAst.RedirectAst {
            return null;
        }

        processDo(): CmdAst.Ast {
            return null;
        }
    }
}
