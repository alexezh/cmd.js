import { CmdLexer } from './cmdlexer';
import { CmdAst } from './cmdast';
export declare module CmdParser {
    import Token = CmdLexer.Token;
    class Parser {
        lexer: CmdLexer.Lexer;
        parse(line: string): CmdAst.Ast;
        processExpression(): CmdAst.Ast;
        processSet(): CmdAst.Ast;
        processCommand(tok: Token): CmdAst.Ast;
        setTaskCmdRoot(node: CmdAst.Ast, val: boolean): void;
        processFor(): CmdAst.Ast;
        processForFileSet(files: boolean, recursive: boolean): CmdAst.Ast;
        processForNumbers(): CmdAst.Ast;
        processForContent(): CmdAst.Ast;
        processSetLocal(): CmdAst.Ast;
        processEndLocal(): CmdAst.Ast;
        processRedirect(): CmdAst.RedirectAst;
        processDo(): CmdAst.Ast;
    }
}
