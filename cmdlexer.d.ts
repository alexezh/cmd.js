export declare module CmdLexer {
    enum TokenId {
        Id = 0,
        Var = 1,
        For = 2,
        If = 3,
        Set = 4,
        SetLocal = 5,
        EndLocal = 6,
        Do = 7,
        String = 8,
        In = 9,
        Chain = 10,
        Pipe = 11,
        Equal = 12,
        Eol = 13,
        LeftParent = 14,
        RightParent = 15,
        Option = 16,
        Params = 17,
    }
    class Token {
        constructor(text: string, id: TokenId, pos: number);
        id: TokenId;
        text: string;
        pos: number;
    }
    class Lexer {
        private _source;
        private _next;
        private _pos;
        private _end;
        private _matcher;
        private _rules;
        constructor(line: string);
        readNext(): Token;
        readNextIf(id: TokenId): Token;
        peekNext(): Token;
        isEnd(): boolean;
        readString(match: string): string;
        private buildMatcher();
        private readNextWorker();
    }
}
